<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'DegordianBattle\\interfaces\\' => array($baseDir . '/src/classes/interfaces'),
    'DegordianBattle\\classes\\' => array($baseDir . '/src/classes'),
    'DegordianBattle\\abstracts\\' => array($baseDir . '/src/classes/abstracts'),
);
